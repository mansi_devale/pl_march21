/*
 * IO.c
 *
 *
 */

#include "main.h"
#include "IO.h"
#include <string.h>
char RX_Buff[20];
/**************************************************************************/
/*
  * @brief After the power up device set GPIO as default configuration according to our requirement.
  * @param None
  * @retval None

**************************************************************************/
void Power_UP_Status(void)
{
	HAL_GPIO_WritePin(GPIOA, USB_VBUS_Pin, GPIO_PIN_RESET);//USB_VBUS  PA9
	HAL_GPIO_WritePin(GPIOC, USB_OTG_HS_ULPI_RST_Pin, GPIO_PIN_RESET);//USB_OTG_HS_ULPI_RST  PC1
	HAL_GPIO_WritePin(GPIOC, AFE_MODE_Pin, GPIO_PIN_SET);//AFE_MODE  PC7
	HAL_GPIO_WritePin(GPIOC, USER_LED_3_Pin, GPIO_PIN_RESET);//USER_LED_3  PC8
	HAL_GPIO_WritePin(GPIOD, ADC_BV_CS_N_Pin, GPIO_PIN_SET);  //ADC_BV_CS_N PD0
	HAL_GPIO_WritePin(GPIOD, SPI3_CS2_Pin, GPIO_PIN_SET);  //SPI3_CS2_Pin  PD1
	HAL_GPIO_WritePin(GPIOD, SPI1_CS2_Pin, GPIO_PIN_SET);  //SPI1_CS2_Pin PD4
	HAL_GPIO_WritePin(GPIOD, AFE_EN_Pin, GPIO_PIN_RESET);  //AFE_EN_Pin PD7
	HAL_GPIO_WritePin(GPIOD, SPI2_CS2_Pin, GPIO_PIN_SET);  //SPI2_CS2_Pin PD9
	HAL_GPIO_WritePin(GPIOD, MUX_SEL1_Pin, GPIO_PIN_RESET);  //MUX_SEL1_Pin PD10
	HAL_GPIO_WritePin(GPIOD, MUX_SEL2_Pin, GPIO_PIN_RESET);  //MUX_SEL2_Pin PD11
	HAL_GPIO_WritePin(GPIOD, MUX_SEL3_Pin, GPIO_PIN_RESET);  //MUX_SEL3_Pin PD13
	HAL_GPIO_WritePin(GPIOD, MUX_SEL4_Pin, GPIO_PIN_RESET);  //MUX_SEL4_Pin PD14
	HAL_GPIO_WritePin(GPIOE, SPI6_CS1_Pin, GPIO_PIN_SET);  //SPI6_CS1_Pin PE0
	HAL_GPIO_WritePin(GPIOE, SPI6_CS3_Pin, GPIO_PIN_SET);  //SPI6_CS3_Pin PE1
	HAL_GPIO_WritePin(GPIOE, SPI4_DAC1_CS_Pin, GPIO_PIN_SET);  //SPI4_DAC1_CS_Pin PE10
	HAL_GPIO_WritePin(GPIOE, USER_LED_1_Pin, GPIO_PIN_RESET);  //USER_LED_1_Pin PE13
//	HAL_GPIO_WritePin(GPIOE, USER_LED_2_Pin, GPIO_PIN_RESET);  //USER_LED_2_Pin PE14
	HAL_GPIO_WritePin(GPIOE, TL1_PD7_Pin, GPIO_PIN_SET);  //TL1_PD7_Pin  PE15
	HAL_GPIO_WritePin(GPIOF, SPI4_DAC2_CS2_Pin, GPIO_PIN_SET);  //SPI4_DAC2_CS2_Pin PF0
	HAL_GPIO_WritePin(GPIOF, VBAT_IMEAS_CS_Pin, GPIO_PIN_SET);  //VBAT_IMEAS_CS_Pin PF1
	HAL_GPIO_WritePin(GPIOF, SPI5_CS2_Pin, GPIO_PIN_SET);  //SPI5_CS2_Pin  PF2
	HAL_GPIO_WritePin(GPIOF, AC_DIS_EN_Pin, GPIO_PIN_RESET);  //AC_DIS_EN_Pin PF3
	HAL_GPIO_WritePin(GPIOF, TP27_Pin, GPIO_PIN_RESET);  //TP27_Pin  PF4
	HAL_GPIO_WritePin(GPIOF, TP26_Pin, GPIO_PIN_RESET);  //TP26_Pin PF5
	HAL_GPIO_WritePin(GPIOF, ADC_IS_CS_N_Pin, GPIO_PIN_SET);  //ADC_IS_CS_N_Pin PF6

	HAL_GPIO_WritePin(GPIOG, DS_EN_Pin, GPIO_PIN_RESET);  //DS_EN_Pin PG0
	HAL_GPIO_WritePin(GPIOG, SPI3_CS3_Pin, GPIO_PIN_SET);  //SPI3_CS3_Pin PG1
//	HAL_GPIO_WritePin(GPIOG, GPIO_EXT_INT_Pin, GPIO_PIN_RESET);  //GPIO_EXT_INT_Pin PG2
	HAL_GPIO_WritePin(GPIOG, SPI5_CS3_Pin, GPIO_PIN_SET);  //SPI5_CS3_Pin PG3
	HAL_GPIO_WritePin(GPIOG, TL_SEL4_Pin, GPIO_PIN_RESET);  //TL_SEL4_Pin PG4
	HAL_GPIO_WritePin(GPIOG, VBAT_VMEAS_CS_Pin, GPIO_PIN_SET);  //VBAT_VMEAS_CS_Pin PG5
	HAL_GPIO_WritePin(GPIOG, MUX_SEL5_Pin, GPIO_PIN_RESET);  //MUX_SEL5_Pin PG6
//	HAL_GPIO_WritePin(GPIOG, SPI1_CS3_PIN, GPIO_PIN_SET);  //SPI1_CS3_PIN PG7
	HAL_GPIO_WritePin(GPIOG, GPIO_EXT_RST_Pin, GPIO_PIN_RESET);  //GPIO_EXT_RST_Pin PG8
	HAL_GPIO_WritePin(GPIOG, TL1_PD6_Pin, GPIO_PIN_SET);  //TL1_PD6_Pin PG15

	HAL_GPIO_WritePin(GPIOH, SPI2_CS3_Pin, GPIO_PIN_SET);  //SPI2_CS3_Pin PH7
	HAL_GPIO_WritePin(GPIOH, TL1_PD6_Pin, GPIO_PIN_RESET);  //TL1_PD6_Pin PH8
	HAL_GPIO_WritePin(GPIOH, DRV_EN_Pin, GPIO_PIN_RESET);  //DRV_EN_Pin PH9
	HAL_GPIO_WritePin(GPIOH, DAC1_TRIG_Pin, GPIO_PIN_SET);  //DAC1_TRIG_Pin PH10
	HAL_GPIO_WritePin(GPIOH, DAC2_TRIG_Pin, GPIO_PIN_SET);  //DAC2_TRIG_Pin PH11
	HAL_GPIO_WritePin(GPIOH, TL_SEL2_Pin, GPIO_PIN_RESET);  //TL_SEL2_Pin PH12
	HAL_GPIO_WritePin(GPIOH, AFE_PSU_RALL_EN_Pin, GPIO_PIN_RESET);  //AFE_PSU_RALL_EN_Pin PH14
	HAL_GPIO_WritePin(GPIOH, SPI6_CAL_CS_Pin, GPIO_PIN_SET);  //SPI6_CAL_CS_Pin PH15



}

/**************************************************************************/
/*
  * @brief After the power up device set GPIO as default configuration according to our requirement.
  * @param None
  * @retval None

**************************************************************************/
void VCP_Enable_Disable_signal(void)
{
	char* token=NULL;
	char* rest=NULL;
	char* SIGNAL_NAME=NULL;
	uint8_t SIGNAL_STATUS=0,c=0,flag;
	if(flag==1)
	{
		flag=0;
		rest=NULL;
		rest=RX_Buff;
		SIGNAL_NAME=NULL;
		SIGNAL_STATUS=0;

		//  strcpy((char *)myBuff,rest);
		// CDC_Transmit_FS(myBuff, strlen((char*)myBuff));
		HAL_Delay(20);
		//  rest=str;
		c=0;
		// memset(myBuff,'\0',strlen((char*)myBuff));
		while((token =strtok_r(rest, " ", &rest)))
		{
			c++;
			//	  printf("%s\n\r",token);
			if(c==1)
			{
				strcpy((char*)SIGNAL_NAME,(char*)token);

				/* strcpy((char *)myBuff,SIGNAL_NAME);
	          		  CDC_Transmit_FS(myBuff, strlen((char*)myBuff));*/
				HAL_Delay(20);
				//emset(myBuff,'\0',strlen((char*)myBuff));
				//	  printf("c1==%s\n\r",token);
			}
			if(c==2)
			{
				SIGNAL_STATUS= atoi(token);
				// strcpy((char *)myBuff,(char*)SIGNAL_STATUS);
				//   		  CDC_Transmit_FS(myBuff, strlen((char*)myBuff));
				HAL_Delay(20);
				// 		  memset(myBuff,'\0',strlen((char*)myBuff));

				//  printf("c2==%d\n\r",SIGNAL_STATUS);
			}
		}
		token=NULL;
		char* str1="USB_VBUS";
		HAL_Delay(20);
		if(strcmp(str1,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOA, USB_VBUS_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOA, USB_VBUS_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str2="USB_OTG_HS_ULPI_RST";
		if(strcmp(str2,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOC, USB_OTG_HS_ULPI_RST_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOC, USB_OTG_HS_ULPI_RST_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str3="AFE_MODE";
		if(strcmp(str3,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOC, AFE_MODE_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOC, AFE_MODE_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str4="USER_LED_3";
		if(strcmp(str4,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOC, USER_LED_3_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOC, USER_LED_3_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str_4="ADC_BV_CS_N";
		if(strcmp(str_4,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOD, ADC_BV_CS_N_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOD, ADC_BV_CS_N_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str5="AFE_EN";
		if(strcmp(str5,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOD, AFE_EN_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOD, AFE_EN_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str6="MUX_SEL1";
		if(strcmp(str6,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOD, MUX_SEL1_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOD, MUX_SEL1_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* str7="MUX_SEL2";
		if(strcmp(str7,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOD, MUX_SEL2_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOD, MUX_SEL2_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}

		char* str8="MUX_SEL3";
		if(strcmp(str8,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOD, MUX_SEL3_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOD, MUX_SEL3_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}

		char* str9="MUX_SEL4";
		if(strcmp(str9,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOD, MUX_SEL4_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOD, MUX_SEL4_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}

		char* STR="USER_LED_1";
		if(strcmp(STR,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOE, USER_LED_1_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOE, USER_LED_1_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
	/*	char* str3="USER_LED_2";
				if(strcmp(str3,(char*)SIGNAL_NAME)==0)
				{
					if(SIGNAL_STATUS==0)
					{
						//  printf("LED OFF\n\r");
						HAL_GPIO_WritePin(GPIOE, USER_LED_2_Pin, GPIO_PIN_RESET);
					}
					if(SIGNAL_STATUS==1)
					{
						HAL_GPIO_WritePin(GPIOE, USER_LED_2_Pin, GPIO_PIN_SET);
						//printf("LED ON\n\r");
					}
				} */
		char* STR1="TL1_PD7";
		if(strcmp(STR1,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOE, TL1_PD7_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOE, TL1_PD7_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* STR2="VBAT_IMEAS_CS";
		if(strcmp(STR2,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOF, VBAT_IMEAS_CS_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOF, VBAT_IMEAS_CS_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* STR3="AC_DIS_EN";
		if(strcmp(STR3,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOF, AC_DIS_EN_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOF, AC_DIS_EN_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* STR4="TP27";
		if(strcmp(STR4,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOF, TP27_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOF, TP27_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* STR5="TP26";
		if(strcmp(STR5,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOF, TP26_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOF, TP26_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* STR6="ADC_IS_CS_N";
		if(strcmp(STR6,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOF, ADC_IS_CS_N_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOF, ADC_IS_CS_N_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char*STR7="DS_EN";
		if(strcmp(STR7,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOG, DS_EN_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOG, DS_EN_Pin, GPIO_PIN_SET);
				//printf("LED ON\n\r");
			}
		}
		char* STR8="GPIO_EXT_INT";
		if(strcmp(STR8,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
	//			HAL_GPIO_WritePin(GPIOG,GPIO_EXT_INT_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
		//		HAL_GPIO_WritePin(GPIOG, GPIO_EXT_INT_Pin, GPIO_PIN_SET);
			}
		}
		char* STR9="TL_SEL4";
		if(strcmp(STR9,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOG, TL_SEL4_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOG, TL_SEL4_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_1="VBAT_VMEAS_CS";
		if(strcmp(STR_1,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOG, VBAT_VMEAS_CS_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOG, VBAT_VMEAS_CS_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_2="MUX_SEL5";
		if(strcmp(STR_2,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOG, MUX_SEL5_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOG, MUX_SEL5_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_3="GPIO_EXT_RST";
		if(strcmp(STR_3,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOG, GPIO_EXT_RST_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOG, GPIO_EXT_RST_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_4="TL1_PD6";
		if(strcmp(STR_4,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOG, TL1_PD6_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOG, TL1_PD6_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_5="DRV_EN";
		if(strcmp(STR_5,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOH, DRV_EN_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOH, DRV_EN_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_6="DAC1_TRIG";
		if(strcmp(STR_6,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOH, DAC1_TRIG_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOH, DAC1_TRIG_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_7="DAC2_TRIG";
		if(strcmp(STR_7,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOH, DAC2_TRIG_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOH, DAC2_TRIG_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_8="TL_SEL2";
		if(strcmp(STR_8,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOH, TL_SEL2_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOH, TL_SEL2_Pin, GPIO_PIN_SET);
			}
		}
		char* STR_9="AFE_PSU_RALL_EN";
		if(strcmp(STR_9,(char*)SIGNAL_NAME)==0)
		{
			if(SIGNAL_STATUS==0)
			{
				//  printf("LED OFF\n\r");
				HAL_GPIO_WritePin(GPIOH, AFE_PSU_RALL_EN_Pin, GPIO_PIN_RESET);
			}
			if(SIGNAL_STATUS==1)
			{
				HAL_GPIO_WritePin(GPIOH, AFE_PSU_RALL_EN_Pin, GPIO_PIN_SET);
			}
		}
		char* STR1_1="SPI6_CAL_CS";
				if(strcmp(STR1_1,(char*)SIGNAL_NAME)==0)
				{
					if(SIGNAL_STATUS==0)
					{
						//  printf("LED OFF\n\r");
						HAL_GPIO_WritePin(GPIOH, SPI6_CAL_CS_Pin, GPIO_PIN_RESET);
					}
					if(SIGNAL_STATUS==1)
					{
						HAL_GPIO_WritePin(GPIOH, SPI6_CAL_CS_Pin, GPIO_PIN_SET);
					}
				}
		memset(RX_Buff,'\0',sizeof(RX_Buff));

	}
}

/**************************************************************************/
/*
  * @brief Allows for induvidaul Pins to setup as input or output and select
  *        if input has Pull Up or pull down internal resistors
  * @param uint16_t pin-Select the pin,
  *        uint8_t mode[-set the mode I/p,O/p,Pull up or Pull down
  * @retval None

**************************************************************************/


void remotepinMode(uint16_t pin, uint8_t mode)
{
	uint8_t  config_data;
	uint8_t  pullup_config_data;
	uint8_t pullup_value_data;
	uint8_t PCAL6524_CONFIGURATION;
	uint8_t PCAL6524_RESISTOR_PULL_ENABLE;
	uint8_t PCAL6524_RESISTOR_PULL_SELECTION;
	//Determine wich bank of pins the requested pin is in
	if (pin == P_EN_P15v || pin == P_EN_N15v || pin == P_EN_N5v || pin == GPIO1_FAN || pin ==DAC1_RST || pin == DAC2_RST || pin ==DS_PD2 ||pin == DS_PD1) //If pin is in the First bank
	{
		PCAL6524_CONFIGURATION = PCAL6524_CONFIGURATION_PORT_0;
		PCAL6524_RESISTOR_PULL_ENABLE = PCAL6524_RESISTOR_PULL_ENABLE_PORT_0;
		PCAL6524_RESISTOR_PULL_SELECTION = PCAL6524_RESISTOR_PULL_SELECTION_PORT_0;
	}
	else if (pin == GPIOA0 || pin == GPIOA1 || pin == GPIOA2 || pin == GPIOA3 ||pin == CH1_PD1 || pin == CH2_PD1 || pin == CH3_PD1|| pin == CH4_PD1) //If pin is in the Second bank
	{
		PCAL6524_CONFIGURATION = PCAL6524_CONFIGURATION_PORT_1;
		PCAL6524_RESISTOR_PULL_ENABLE = PCAL6524_RESISTOR_PULL_ENABLE_PORT_1;
		PCAL6524_RESISTOR_PULL_SELECTION = PCAL6524_RESISTOR_PULL_SELECTION_PORT_1;
	}
	else if (pin == GPIOB0 || pin == GPIOB1 || pin == GPIOB2 || pin == TL_PD1 || pin == TL_PD2 || pin == TL_PD3 || pin ==TL_PD4 || pin == TL_PD5) //If pin is in the Third bank
	{
		PCAL6524_CONFIGURATION = PCAL6524_CONFIGURATION_PORT_2;
		PCAL6524_RESISTOR_PULL_ENABLE = PCAL6524_RESISTOR_PULL_ENABLE_PORT_2;
		PCAL6524_RESISTOR_PULL_SELECTION = PCAL6524_RESISTOR_PULL_SELECTION_PORT_2;
	}
	//read the current Input/output configuration settings for the given bank of pins
/*	config_data = readRegister(m_i2cAddress,PCAL6524_CONFIGURATION);*/
    HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);

	//read the current pullup enable configuration settings for the given bank of pins
/*	pullup_config_data = readRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_ENABLE);*/
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_ENABLE, 1, &pullup_config_data, 1, 100);
//	HAL_I2C_Mem_Read(hi2c, DevAddress, MemAddress, MemAddSize, pData, Size, Timeout);
	//read the current pullup enable configuration settings for the given bank of pins
	//pullup_value_data = readRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_SELECTION);
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_SELECTION, 1, &pullup_value_data, 1, 100);
	/*This section is used to determine what mode of operation is selected
		Input
		Output
		Input With PullUp Resistor Active
		Input with PullDown Resistor Active
	*/
	if (mode == INPUT)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data | (uint8_t)pin;
		//Write the new configuration back to the Resistor
	//	writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);  //Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		return;
	}
	else if (mode == OUTPUT)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data ^ (uint8_t)pin;//Combine the current configuration with the request pin to ensure that only that pin has chaned
		//Write the new configuration back to the Resistor
	//	writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);  //Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		return;
	}
	else if (mode == INPUT_PULLUP)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data | (uint8_t)pin;
		//Write the new configuration back to the Resistor
		//writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);//Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		//This is used to configure the pullup/down resistor are actived and configured correctly
		pullup_config_data = pullup_config_data | (uint8_t)pin;
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_ENABLE,pullup_config_data);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_ENABLE, 1, &pullup_config_data, 1, 100);
		pullup_value_data = pullup_value_data | (uint8_t)pin;
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0,PCAL6524_RESISTOR_PULL_SELECTION, 1,&pullup_value_data, 1, 100);
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_SELECTION,pullup_value_data);
		return;
	}
	else if (mode == INPUT_PULLDOWN)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data | pin;
		//Write the new configuration back to the Resistor
	//	writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);//Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		//This is used to configure the pullup/down resistor are actived and configured correctly
		pullup_config_data = pullup_config_data | (uint8_t)pin;
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_ENABLE,pullup_config_data);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_ENABLE, 1, &pullup_config_data, 1, 100);
		pullup_value_data = pullup_value_data ^ (uint8_t)pin;
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_SELECTION,pullup_config_data);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_SELECTION, 1, &pullup_value_data, 1, 100);
		return;
	}
	return;
	}

/**************************************************************************/
/*
  * @brief Allows for individual Pins designated as outputs to
  *        be set as High or Low
  * @param uint16_t pin-Select the pin,
  *        uint8_t mode-Set the port mode as HIGH/LOW
  * @retval None

**************************************************************************/
void remotedigitalWrite(uint16_t pin, uint8_t mode){
	uint8_t output_reg_value;
	uint8_t PCAL6524_OUTPUT;

	//Determine wich bank of pins the requested pin is in
	if (pin == P_EN_P15v || pin == P_EN_N15v || pin == P_EN_N5v || pin == GPIO1_FAN || pin ==DAC1_RST || pin == DAC2_RST || pin ==DS_PD2 ||pin == DS_PD1)
	{
		PCAL6524_OUTPUT = PCAL6524_OUTPUT_PORT_0;
	}
	else if (pin == GPIOA0 || pin == GPIOA1 || pin == GPIOA2 || pin == GPIOA3 ||pin == CH1_PD1 || pin == CH2_PD1 || pin == CH3_PD1|| pin == CH4_PD1)
	{
		PCAL6524_OUTPUT = PCAL6524_OUTPUT_PORT_1;
	}
	else if (pin == GPIOB0 || pin == GPIOB1 || pin == GPIOB2 || pin == TL_PD1 || pin == TL_PD2 || pin == TL_PD3 || pin ==TL_PD4 || pin == TL_PD5)
	{
		PCAL6524_OUTPUT = PCAL6524_OUTPUT_PORT_2;
	}

	// Read the currnt Value of out the ouput register
//	output_reg_value = readRegister(m_i2cAddress,PCAL6524_OUTPUT);
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_OUTPUT, 1, &output_reg_value, 1, 100);
	//Deterime if Pin is being asked to go hi or to go low and set only that pins value;
	if (mode == HIGH)
	{
		output_reg_value = output_reg_value | (uint8_t)pin;
		//writeRegister(m_i2cAddress,PCAL6524_OUTPUT,output_reg_value);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_OUTPUT, 1, &output_reg_value, 1, 100);
		return;
	}
	else if (mode == LOW){
		output_reg_value = output_reg_value & ~((uint8_t)pin);
		//writeRegister(m_i2cAddress,PCAL6524_OUTPUT,output_reg_value);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_OUTPUT, 1, &output_reg_value, 1, 100);
		return;
	}
	return;
}
/**************************************************************************/
/*
  * @brief Reading the current status of Pin
  * @param uint16_t pin-Select the pin,
  * @retval None

**************************************************************************/
uint8_t remotedigitalRead(uint16_t pin)
{
	uint8_t input_reg_data;
	uint8_t PCAL6524_INPUT;
	//Determine wich bank of pins the requested pin is in
	if (pin == P_EN_P15v || pin == P_EN_N15v || pin == P_EN_N5v || pin == GPIO1_FAN || pin ==DAC1_RST || pin == DAC2_RST || pin ==DS_PD2 ||pin == DS_PD1)
	{
		PCAL6524_INPUT = PCAL6524_INPUT_PORT_0;
	}
	else if (pin == GPIOA0 || pin == GPIOA1 || pin == GPIOA2 || pin == GPIOA3 ||pin == CH1_PD1 || pin == CH2_PD1 || pin == CH3_PD1|| pin == CH4_PD1)
	{
		PCAL6524_INPUT = PCAL6524_INPUT_PORT_1;
	}
	else if (pin == GPIOB0 || pin == GPIOB1 || pin == GPIOB2 || pin == TL_PD1 || pin == TL_PD2 || pin == TL_PD3 || pin ==TL_PD4 || pin == TL_PD5)
	{
		PCAL6524_INPUT = PCAL6524_INPUT_PORT_2;
	}
	//read the input register data
	//input_reg_data = readRegister(m_i2cAddress,PCAL6524_INPUT);
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_INPUT, 1, &input_reg_data, 1, 100);
	//Isolate the reqested pin value from all other values
	input_reg_data = input_reg_data & (uint8_t)pin;

	//Bit Shift the resulting data over so the pin's requested value becomes the LSB
	switch ((uint8_t)pin)
	{
		case (0x01):  //For Pins Px_0
		{
			return input_reg_data;
		}
		case (0x02)://For Pins Px_1
		{
			return input_reg_data>>1;
		}
		case (0x04)://For Pins Px_2
		{
			return input_reg_data>>2;
		}
		case (0x08)://For Pins Px_3
		{
			return input_reg_data>>3;
		}
		case (0x10)://For Pins Px_4
		{
			return input_reg_data>>4;
		}
		case (0x20)://For Pins Px_5
		{
			return input_reg_data>>5;
		}
		case (0x40)://For Pins Px_6
		{
			return input_reg_data>>6;
		}
		case (0x80)://For Pins Px_7
		{
			return input_reg_data>>7;
		}
	}
}
