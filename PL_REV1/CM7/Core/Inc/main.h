/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define UART4_RX_Pin GPIO_PIN_8
#define UART4_RX_GPIO_Port GPIOB
#define SPI1_SCK_Pin GPIO_PIN_11
#define SPI1_SCK_GPIO_Port GPIOG
#define SPI1_MISO_Pin GPIO_PIN_9
#define SPI1_MISO_GPIO_Port GPIOG
#define SPI2_SCK_Pin GPIO_PIN_3
#define SPI2_SCK_GPIO_Port GPIOD
#define SPI3_CS2_Pin GPIO_PIN_1
#define SPI3_CS2_GPIO_Port GPIOD
#define SPI6_CS1_Pin GPIO_PIN_0
#define SPI6_CS1_GPIO_Port GPIOE
#define AFE_CLFLG_Pin GPIO_PIN_7
#define AFE_CLFLG_GPIO_Port GPIOB
#define SPI6_SCK_Pin GPIO_PIN_13
#define SPI6_SCK_GPIO_Port GPIOG
#define AFE_EN_Pin GPIO_PIN_7
#define AFE_EN_GPIO_Port GPIOD
#define USART2_TX_Pin GPIO_PIN_5
#define USART2_TX_GPIO_Port GPIOD
#define AFE_PSU_RALL_EN_Pin GPIO_PIN_14
#define AFE_PSU_RALL_EN_GPIO_Port GPIOH
#define I2C3_SCL_Pin GPIO_PIN_8
#define I2C3_SCL_GPIO_Port GPIOA
#define SPI6_CS3_Pin GPIO_PIN_1
#define SPI6_CS3_GPIO_Port GPIOE
#define SPI1_CS2_Pin GPIO_PIN_4
#define SPI1_CS2_GPIO_Port GPIOD
#define ADC_BV_CS_N_Pin GPIO_PIN_0
#define ADC_BV_CS_N_GPIO_Port GPIOD
#define SPI3_MISO_Pin GPIO_PIN_11
#define SPI3_MISO_GPIO_Port GPIOC
#define SPI3_SCK_Pin GPIO_PIN_10
#define SPI3_SCK_GPIO_Port GPIOC
#define UART4_TX_Pin GPIO_PIN_13
#define UART4_TX_GPIO_Port GPIOH
#define USB_ID_Pin GPIO_PIN_10
#define USB_ID_GPIO_Port GPIOA
#define SPI4_MISO_Pin GPIO_PIN_5
#define SPI4_MISO_GPIO_Port GPIOE
#define TL1_PD6_Pin GPIO_PIN_15
#define TL1_PD6_GPIO_Port GPIOG
#define SPI6_MISO_Pin GPIO_PIN_12
#define SPI6_MISO_GPIO_Port GPIOG
#define USART2_RX_Pin GPIO_PIN_6
#define USART2_RX_GPIO_Port GPIOD
#define SPI6_CAL_CS_Pin GPIO_PIN_15
#define SPI6_CAL_CS_GPIO_Port GPIOH
#define USB_VBUS_Pin GPIO_PIN_9
#define USB_VBUS_GPIO_Port GPIOA
#define USER_LED_3_Pin GPIO_PIN_8
#define USER_LED_3_GPIO_Port GPIOC
#define AFE_MODE_Pin GPIO_PIN_7
#define AFE_MODE_GPIO_Port GPIOC
#define SPI4_MOSI_Pin GPIO_PIN_6
#define SPI4_MOSI_GPIO_Port GPIOE
#define I2C3_SDA_Pin GPIO_PIN_9
#define I2C3_SDA_GPIO_Port GPIOC
#define VBAT_IMEAS_CS_Pin GPIO_PIN_1
#define VBAT_IMEAS_CS_GPIO_Port GPIOF
#define SPI4_DAC2_CS2_Pin GPIO_PIN_0
#define SPI4_DAC2_CS2_GPIO_Port GPIOF
#define MUX_SEL5_Pin GPIO_PIN_6
#define MUX_SEL5_GPIO_Port GPIOG
#define VBAT_VMEAS_CS_Pin GPIO_PIN_5
#define VBAT_VMEAS_CS_GPIO_Port GPIOG
#define SPI5_CS2_Pin GPIO_PIN_2
#define SPI5_CS2_GPIO_Port GPIOF
#define GPIO_EXT_RST_Pin GPIO_PIN_8
#define GPIO_EXT_RST_GPIO_Port GPIOG
#define SPI1_CS3_Pin GPIO_PIN_7
#define SPI1_CS3_GPIO_Port GPIOG
#define TL_SEL4_Pin GPIO_PIN_4
#define TL_SEL4_GPIO_Port GPIOG
#define ADC_IS_CS_N_Pin GPIO_PIN_6
#define ADC_IS_CS_N_GPIO_Port GPIOF
#define TP27_Pin GPIO_PIN_4
#define TP27_GPIO_Port GPIOF
#define TP26_Pin GPIO_PIN_5
#define TP26_GPIO_Port GPIOF
#define AC_DIS_EN_Pin GPIO_PIN_3
#define AC_DIS_EN_GPIO_Port GPIOF
#define SPI5_CS3_Pin GPIO_PIN_3
#define SPI5_CS3_GPIO_Port GPIOG
#define MUX_SEL4_Pin GPIO_PIN_14
#define MUX_SEL4_GPIO_Port GPIOD
#define MUX_SEL3_Pin GPIO_PIN_13
#define MUX_SEL3_GPIO_Port GPIOD
#define SPI5_MISO_Pin GPIO_PIN_8
#define SPI5_MISO_GPIO_Port GPIOF
#define SPI5_SCK_Pin GPIO_PIN_7
#define SPI5_SCK_GPIO_Port GPIOF
#define MUX_SEL2_Pin GPIO_PIN_11
#define MUX_SEL2_GPIO_Port GPIOD
#define SPI2_CS2_Pin GPIO_PIN_9
#define SPI2_CS2_GPIO_Port GPIOD
#define SPI2_MISO_Pin GPIO_PIN_14
#define SPI2_MISO_GPIO_Port GPIOB
#define USB_OTG_HS_ULPI_RST_Pin GPIO_PIN_1
#define USB_OTG_HS_ULPI_RST_GPIO_Port GPIOC
#define MUX_SEL1_Pin GPIO_PIN_10
#define MUX_SEL1_GPIO_Port GPIOD
#define TL_SEL3_Pin GPIO_PIN_8
#define TL_SEL3_GPIO_Port GPIOD
#define SPI2_CS3_Pin GPIO_PIN_7
#define SPI2_CS3_GPIO_Port GPIOH
#define DAC2_TRIG_Pin GPIO_PIN_11
#define DAC2_TRIG_GPIO_Port GPIOH
#define DRV_EN_Pin GPIO_PIN_9
#define DRV_EN_GPIO_Port GPIOH
#define SPI3_CS3_Pin GPIO_PIN_1
#define SPI3_CS3_GPIO_Port GPIOG
#define TL_SEL1_Pin GPIO_PIN_8
#define TL_SEL1_GPIO_Port GPIOH
#define DAC1_TRIG_Pin GPIO_PIN_10
#define DAC1_TRIG_GPIO_Port GPIOH
#define TL_SEL2_Pin GPIO_PIN_12
#define TL_SEL2_GPIO_Port GPIOH
#define DS_EN_Pin GPIO_PIN_0
#define DS_EN_GPIO_Port GPIOG
#define SPI4_SCK_Pin GPIO_PIN_12
#define SPI4_SCK_GPIO_Port GPIOE
#define USER_LED_1_Pin GPIO_PIN_13
#define USER_LED_1_GPIO_Port GPIOE
#define TL1_PD7_Pin GPIO_PIN_15
#define TL1_PD7_GPIO_Port GPIOE
#define SPI4_DAC1_CS_Pin GPIO_PIN_10
#define SPI4_DAC1_CS_GPIO_Port GPIOE
void   MX_GPIO_Init(void);
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
