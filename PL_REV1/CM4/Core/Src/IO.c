/*
 * IO.c
 *
 *
 */


#include "main.h"
#include "IO.h"


uint8_t ALERT_GEN;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin==THERM1_ALERT_INT_Pin)
	{
		ALERT_GEN=1;
	}
}
void ALERT_GENERATE(void)
{
	if(ALERT_GEN==1)
	{
		remotepinMode(GPIO1_FAN, OUTPUT);
	    remotedigitalWrite(GPIO1_FAN, HIGH);
	}
}
/**************************************************************************/
/*
  * @brief Allows for induvidaul Pins to setup as input or output and select
  *        if input has Pull Up or pull down internal resistors
  * @param uint16_t pin-Select the pin,
  *        uint8_t mode[-set the mode I/p,O/p,Pull up or Pull down
  * @retval None

**************************************************************************/


void remotepinMode(uint16_t pin, uint8_t mode)
{
	uint8_t  config_data;
	uint8_t  pullup_config_data;
	uint8_t pullup_value_data;
	uint8_t PCAL6524_CONFIGURATION;
	uint8_t PCAL6524_RESISTOR_PULL_ENABLE;
	uint8_t PCAL6524_RESISTOR_PULL_SELECTION;
	//Determine wich bank of pins the requested pin is in
	if (pin == P_EN_P15v || pin == P_EN_N15v || pin == P_EN_N5v || pin == GPIO1_FAN || pin ==DAC1_RST || pin == DAC2_RST || pin ==DS_PD2 ||pin == DS_PD1) //If pin is in the First bank
	{
		PCAL6524_CONFIGURATION = PCAL6524_CONFIGURATION_PORT_0;
		PCAL6524_RESISTOR_PULL_ENABLE = PCAL6524_RESISTOR_PULL_ENABLE_PORT_0;
		PCAL6524_RESISTOR_PULL_SELECTION = PCAL6524_RESISTOR_PULL_SELECTION_PORT_0;
	}
	else if (pin == GPIOA0 || pin == GPIOA1 || pin == GPIOA2 || pin == GPIOA3 ||pin == CH1_PD1 || pin == CH2_PD1 || pin == CH3_PD1|| pin == CH4_PD1) //If pin is in the Second bank
	{
		PCAL6524_CONFIGURATION = PCAL6524_CONFIGURATION_PORT_1;
		PCAL6524_RESISTOR_PULL_ENABLE = PCAL6524_RESISTOR_PULL_ENABLE_PORT_1;
		PCAL6524_RESISTOR_PULL_SELECTION = PCAL6524_RESISTOR_PULL_SELECTION_PORT_1;
	}
	else if (pin == GPIOB0 || pin == GPIOB1 || pin == GPIOB2 || pin == TL_PD1 || pin == TL_PD2 || pin == TL_PD3 || pin ==TL_PD4 || pin == TL_PD5) //If pin is in the Third bank
	{
		PCAL6524_CONFIGURATION = PCAL6524_CONFIGURATION_PORT_2;
		PCAL6524_RESISTOR_PULL_ENABLE = PCAL6524_RESISTOR_PULL_ENABLE_PORT_2;
		PCAL6524_RESISTOR_PULL_SELECTION = PCAL6524_RESISTOR_PULL_SELECTION_PORT_2;
	}
	//read the current Input/output configuration settings for the given bank of pins
/*	config_data = readRegister(m_i2cAddress,PCAL6524_CONFIGURATION);*/
    HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);

	//read the current pullup enable configuration settings for the given bank of pins
/*	pullup_config_data = readRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_ENABLE);*/
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_ENABLE, 1, &pullup_config_data, 1, 100);
//	HAL_I2C_Mem_Read(hi2c, DevAddress, MemAddress, MemAddSize, pData, Size, Timeout);
	//read the current pullup enable configuration settings for the given bank of pins
	//pullup_value_data = readRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_SELECTION);
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_SELECTION, 1, &pullup_value_data, 1, 100);
	/*This section is used to determine what mode of operation is selected
		Input
		Output
		Input With PullUp Resistor Active
		Input with PullDown Resistor Active
	*/
	if (mode == INPUT)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data | (uint8_t)pin;
		//Write the new configuration back to the Resistor
	//	writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);  //Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		return;
	}
	else if (mode == OUTPUT)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data ^ (uint8_t)pin;//Combine the current configuration with the request pin to ensure that only that pin has chaned
		//Write the new configuration back to the Resistor
	//	writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);  //Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		return;
	}
	else if (mode == INPUT_PULLUP)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data | (uint8_t)pin;
		//Write the new configuration back to the Resistor
		//writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);//Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		//This is used to configure the pullup/down resistor are actived and configured correctly
		pullup_config_data = pullup_config_data | (uint8_t)pin;
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_ENABLE,pullup_config_data);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_ENABLE, 1, &pullup_config_data, 1, 100);
		pullup_value_data = pullup_value_data | (uint8_t)pin;
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0,PCAL6524_RESISTOR_PULL_SELECTION, 1,&pullup_value_data, 1, 100);
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_SELECTION,pullup_value_data);
		return;
	}
	else if (mode == INPUT_PULLDOWN)
	{
		//Combine the current configuration with the request pin to ensure that only that pin has chaned
		config_data = config_data | pin;
		//Write the new configuration back to the Resistor
	//	writeRegister(m_i2cAddress,PCAL6524_CONFIGURATION,config_data);//Write the new configuration back to the Resistor
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_CONFIGURATION, 1, &config_data, 1, 100);
		//This is used to configure the pullup/down resistor are actived and configured correctly
		pullup_config_data = pullup_config_data | (uint8_t)pin;
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_ENABLE,pullup_config_data);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_ENABLE, 1, &pullup_config_data, 1, 100);
		pullup_value_data = pullup_value_data ^ (uint8_t)pin;
	//	writeRegister(m_i2cAddress,PCAL6524_RESISTOR_PULL_SELECTION,pullup_config_data);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_RESISTOR_PULL_SELECTION, 1, &pullup_value_data, 1, 100);
		return;
	}
	return;
	}

/**************************************************************************/
/*
  * @brief Allows for individual Pins designated as outputs to
  *        be set as High or Low
  * @param uint16_t pin-Select the pin,
  *        uint8_t mode-Set the port mode as HIGH/LOW
  * @retval None

**************************************************************************/
void remotedigitalWrite(uint16_t pin, uint8_t mode){
	uint8_t output_reg_value;
	uint8_t PCAL6524_OUTPUT;

	//Determine wich bank of pins the requested pin is in
	if (pin == P_EN_P15v || pin == P_EN_N15v || pin == P_EN_N5v || pin == GPIO1_FAN || pin ==DAC1_RST || pin == DAC2_RST || pin ==DS_PD2 ||pin == DS_PD1)
	{
		PCAL6524_OUTPUT = PCAL6524_OUTPUT_PORT_0;
	}
	else if (pin == GPIOA0 || pin == GPIOA1 || pin == GPIOA2 || pin == GPIOA3 ||pin == CH1_PD1 || pin == CH2_PD1 || pin == CH3_PD1|| pin == CH4_PD1)
	{
		PCAL6524_OUTPUT = PCAL6524_OUTPUT_PORT_1;
	}
	else if (pin == GPIOB0 || pin == GPIOB1 || pin == GPIOB2 || pin == TL_PD1 || pin == TL_PD2 || pin == TL_PD3 || pin ==TL_PD4 || pin == TL_PD5)
	{
		PCAL6524_OUTPUT = PCAL6524_OUTPUT_PORT_2;
	}

	// Read the currnt Value of out the ouput register
//	output_reg_value = readRegister(m_i2cAddress,PCAL6524_OUTPUT);
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_OUTPUT, 1, &output_reg_value, 1, 100);
	//Deterime if Pin is being asked to go hi or to go low and set only that pins value;
	if (mode == HIGH)
	{
		output_reg_value = output_reg_value | (uint8_t)pin;
		//writeRegister(m_i2cAddress,PCAL6524_OUTPUT,output_reg_value);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_OUTPUT, 1, &output_reg_value, 1, 100);
		return;
	}
	else if (mode == LOW){
		output_reg_value = output_reg_value & ~((uint8_t)pin);
		//writeRegister(m_i2cAddress,PCAL6524_OUTPUT,output_reg_value);
		HAL_I2C_Mem_Write(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_OUTPUT, 1, &output_reg_value, 1, 100);
		return;
	}
	return;
}
/**************************************************************************/
/*
  * @brief Reading the current status of Pin
  * @param uint16_t pin-Select the pin,
  * @retval None

**************************************************************************/
uint8_t remotedigitalRead(uint16_t pin)
{
	uint8_t input_reg_data;
	uint8_t PCAL6524_INPUT;
	//Determine wich bank of pins the requested pin is in
	if (pin == P_EN_P15v || pin == P_EN_N15v || pin == P_EN_N5v || pin == GPIO1_FAN || pin ==DAC1_RST || pin == DAC2_RST || pin ==DS_PD2 ||pin == DS_PD1)
	{
		PCAL6524_INPUT = PCAL6524_INPUT_PORT_0;
	}
	else if (pin == GPIOA0 || pin == GPIOA1 || pin == GPIOA2 || pin == GPIOA3 ||pin == CH1_PD1 || pin == CH2_PD1 || pin == CH3_PD1|| pin == CH4_PD1)
	{
		PCAL6524_INPUT = PCAL6524_INPUT_PORT_1;
	}
	else if (pin == GPIOB0 || pin == GPIOB1 || pin == GPIOB2 || pin == TL_PD1 || pin == TL_PD2 || pin == TL_PD3 || pin ==TL_PD4 || pin == TL_PD5)
	{
		PCAL6524_INPUT = PCAL6524_INPUT_PORT_2;
	}
	//read the input register data
	//input_reg_data = readRegister(m_i2cAddress,PCAL6524_INPUT);
	HAL_I2C_Mem_Read(&IO_I2C, PCAL6524_ADDRESS_0, PCAL6524_INPUT, 1, &input_reg_data, 1, 100);
	//Isolate the reqested pin value from all other values
	input_reg_data = input_reg_data & (uint8_t)pin;

	//Bit Shift the resulting data over so the pin's requested value becomes the LSB
	switch ((uint8_t)pin)
	{
		case (0x01):  //For Pins Px_0
		{
			return input_reg_data;
		}
		case (0x02)://For Pins Px_1
		{
			return input_reg_data>>1;
		}
		case (0x04)://For Pins Px_2
		{
			return input_reg_data>>2;
		}
		case (0x08)://For Pins Px_3
		{
			return input_reg_data>>3;
		}
		case (0x10)://For Pins Px_4
		{
			return input_reg_data>>4;
		}
		case (0x20)://For Pins Px_5
		{
			return input_reg_data>>5;
		}
		case (0x40)://For Pins Px_6
		{
			return input_reg_data>>6;
		}
		case (0x80)://For Pins Px_7
		{
			return input_reg_data>>7;
		}
	}
}
