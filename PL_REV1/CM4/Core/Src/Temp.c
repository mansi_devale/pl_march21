/*
 * Temp.c
 *
 *
 */

//#include "main.h"
#include "Temp.h"




extern I2C_HandleTypeDef hi2c1;
uint8_t Local_temp=0;
uint8_t Remote_Temp_HB=0,Remote_Temp_LB=0;
int LOCAL_TEMP=0,EXTENDED_HB=0,EXTENDED_LB=0;
int EXT_FLAG=0;

extern void remotepinMode(uint16_t pin, uint8_t mode);
extern uint8_t remotedigitalRead(uint16_t pin);
extern void remotedigitalWrite(uint16_t pin, uint8_t mode);

uint8_t Flag_STATUS[7];
int16_t  TEMP_Value[10];


#define LHIGH       Flag_STATUS[6]                 //1 when local high temperature limit is tripped
#define LLOW        Flag_STATUS[5]                 //1 when local low temperature limit is tripped
#define RHIGH       Flag_STATUS[4]                 //1 when remote high temperature limit is tripped
#define RLOW        Flag_STATUS[3]                 //1 when remote low temperature limit is tripped
#define OPEN        Flag_STATUS[2]                 //1 when remote sensor is an open circuit
#define RTHRM       Flag_STATUS[1]                 //1 when remote THERM limit is tripped
#define LTHRM       Flag_STATUS[0]                 //1 when local THERM limit is tripped


int8_t temp8[4];
            /* 0: local low limit
			   1: local high limit
			   2: local critical limit
			   3: remote critical limit */
int16_t temp11[5];
             /* 0: remote input
			   1: remote low limit
			   2: remote high limit
			   3: remote offset
			   4: local input */
uint8_t temp_hyst;
int temp_from_u8(uint8_t data, uint8_t val);
int temp_from_u16(uint8_t data, uint16_t val);



uint8_t TEMP_Powerup_READ(void)
{
	uint8_t FALGS=0;

	EXT_FLAG=0;
	uint8_t h, l;
	if(HAL_OK!=HAL_I2C_IsDeviceReady(&TEMP_I2C, TEMP_ADDR, 5,100))
	{
		//printf("ERROR in Temp I2c reading\n\r");
	}

	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C, TEMP_ADDR, RW_Local_Temp_LL, 1,(uint8_t *)&temp8[0], 1, 100))
	{
		//printf("Error in reading Local Temperature LOW Limit\n\r");
	}
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_Local_Temp_HL, 1, (uint8_t *)&temp8[1], 1, 100))
	{
		//printf("Error in reading Local Temperature HIGH Limit\n\r");
	}
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_Local_THERM_Limit ,1, (uint8_t *)&temp8[2], 1, 100))
	{
		//printf("Error in reading Local THERM Limit\n\r");
	}
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_Ext_THERM_Limit, 1, (uint8_t *)&temp8[3], 1, 100))
		{
			//printf("Error in reading Local THERM Limit\n\r");
		}
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_THERM_Hysteresis , 1, &temp_hyst, 1, 100))
			{
				//printf("Error in reading THERM Hysteresis\n\r");
			}

	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,  Local_Temp_Value , 1, &h, 1, 100))
	{
		//printf("Error in reading THERM Hysteresis\n\r");
	}
	temp11[4] = h << 8;
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_Ext_Temp_Value_LB  , 1, (uint8_t *)&temp11[0], 1, 100))
	{
		//printf("Error in readingExternal Temperature Value Low Byte\n\r");
	}


	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_Ext_Temp_LL_HB  , 1,  &h, 1, 100))
	{
		//printf("Error in reading External Temperature Low Limit High Byte\n\r");
	}
	temp11[1] = h << 8;
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,RW_Ext_Temp_LL_LB  , 1,  &l, 1, 100))
	{
		//printf("Error in reading External Temperature Low Limit Low Byte\n\r");
	}
    temp11[1] |= l;


	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR, RW_Ext_Temp_HL_HB   , 1,  &h, 1, 100))
	{
		//printf("Error in reading External Temperature High Limit High Byte\n\r");
	}
	temp11[2] = h << 8;
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,RW_Ext_Temp_HL_LB  , 1,  &l, 1, 100))
	{
		//printf("Error in reading External Temperature High Limit Low Byte\n\r");
	}
    temp11[2] |= l;


	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C, TEMP_ADDR,RW_Ext_Temp_Offset_HB, 1,  &h, 1, 100))

	{
		//printf("Error in reading External Temperature High Limit High Byte\n\r");
	}
	if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,RW_Ext_Temp_Offset_LB,1,&l,1,100))
	{
		//printf("Error in reading External Temperature High Limit Low Byte\n\r");
	}

	temp11[3] = (h << 8) | l;

	 if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,RW_Configuration, 1, &FALGS, 1, 100))
		       {
		    	// printf("Error in reading local Temp\n\r");
		       }
		   EXT_FLAG=(1<<2)&FALGS;
		   if(EXT_FLAG > 0)
		     {
			    EXT_FLAG=1;
		     }
		  else
			   EXT_FLAG=0;



/*	 if(HAL_OK!=HAL_I2C_Mem_Write(&TEMP_I2C,TEMP_ADDR,WR_Configuration, 1, &Configuration, 1, 100))
	       {
	    	// printf("Error in Writing in Configuration\n\r");
	       }

	 if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,RW_Configuration, 1, &FALGS, 1, 100))
	       {
	    	// printf("Error in reading local Temp\n\r");
	       }
	   EXT_FLAG=(1<<2)&FALGS;
	   if(EXT_FLAG > 0)
	     {
		    EXT_FLAG=1;
	     }
	  else
		   EXT_FLAG=0;

     if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,Local_Temp_Value, 1, &Local_temp, 1, 100))
       {
    	// printf("Error in reading local Temp\n\r");
       }
       LOCAL_TEMP= temp_from_u8_adt7461(EXT_FLAG, Local_temp);
   //    printf("Local Temp=%d\n",LOCAL_TEMP);

     if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,Ext_Temp_Value_HB, 1, &Remote_Temp_HB, 1, 100))
         {
        	// printf("Error in reading Remote Temp\n\r");
         }
       EXTENDED_HB = temp_to_u8_adt7461(EXT_FLAG, Remote_Temp_HB);

     if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,Ext_Temp_Value_HB, 1, &Remote_Temp_LB, 1, 100))
              {
             	// printf("Error in reading Remote Temp\n\r");
              }
      EXTENDED_LB= temp_to_u8_adt7461(EXT_FLAG, Remote_Temp_LB);*/
return EXT_FLAG;
}

void TEMP_Read(void)
{
	uint8_t data=0,Val=0;
	data=TEMP_Powerup_READ();
	int temp8=0,i,temp16=0;
	for(i=0;i<4;i++)
	{
//	Val=temp8[i];
	//strcpy(Val,temp8[i]);
    temp8= temp_from_u8(data,Val);
    Val=0;
    printf("Temp=%d\n\r",temp8);
	}

	for(i=1;i<=4;i++)
		{
		Val=temp11[i];
		temp16= temp_from_u16(data,Val);
	    Val=0;
	    printf("Temp=%d\n\r",temp16);
		}
}


void STATUS_TEMP(void)
{
	 uint8_t STATUS_Value=0;

    memset((uint8_t *)Flag_STATUS,'\0',8);
	 if(HAL_OK!=HAL_I2C_Mem_Read(&TEMP_I2C,TEMP_ADDR,STATUS_RD_TEMP, 1, &STATUS_Value, 1, 100))
	              {
	             	// printf("Error in reading STATUS TEMP\n\r");
	              }
	 LTHRM=(1<<2)&STATUS_Value;
	 RTHRM=(1<<2)&STATUS_Value;
	 OPEN=(1<<2)&STATUS_Value;
	 RLOW=(1<<2)&STATUS_Value;
	 RHIGH=(1<<2)&STATUS_Value;
	 LLOW =(1<<2)&STATUS_Value;
	 LHIGH=(1<<2)&STATUS_Value;
	 if((LHIGH>=1)||(LLOW >=1)||(RHIGH >=1)||(RLOW >=1)||(OPEN >=1))
	 {
		 printf("ALERT is generated\n\r");
		 if((LHIGH >=1))
			            printf("local high temperature limit is tripped\n\r");
		 if((LLOW >=1))
		 			    printf("when local low temperature limit is tripped\n\r");
		 if((RHIGH >=1))
				 	    printf("when remote high temperature limit is tripped\n\r");
		 if((RLOW >=1))
				 	    printf("when remote low temperature limit is tripped\n\r");
		 if((OPEN >=1))
				 	    printf("when remote sensor is an open circuit\n\r");
	 }
	 if((LTHRM>=1)||(RTHRM >=1))
	 {
		 printf("THERM interrupt is generated\n\r");
				 if((LTHRM >=1))
					            printf("when remote THERM limit is tripped\n\r");
				 if((RTHRM >=1))
				 			    printf("when local THERM limit is tripped\n\r");
	 }
}


/*
 * ADT7461 attempts to write values that are outside the range
 * 0 < temp < 127 are treated as the boundary value.
 *
 * ADT7461 in "extended mode" operation uses unsigned integers offset by
 * 64 (e.g., 0 -> -64 degC).  The range is restricted to -64..191 degC.
 */

int temp_from_s8(int8_t val)
{
	return val * 1000;
}
 uint8_t hyst_to_reg(long val)
{
	if (val <= 0)
		return 0;
	if (val >= 30500)
		return 31;
	return (val + 500) / 1000;
}
 int temp_from_u8(uint8_t data, uint8_t val)
 {
 	if (data & FLAG_ADT7461_EXT)
 		return (val - 64) * 1000;
 	else
 		return val * 1000;;
 }
int temp_from_u16(uint8_t data, uint16_t val)
{
	if (data & FLAG_ADT7461_EXT)
		return (val - 0x4000) / 64 * 250;
	else
		return val / 32 * 125;
}
uint8_t temp_to_u8(uint8_t data, long val)
{
	if (data & FLAG_ADT7461_EXT) {
		if (val <= -64000)
			return 0;
		if (val >= 191000)
			return 0xFF;
		return (val + 500 + 64000) / 1000;
	} else {
		if (val <= 0)
			return 0;
		if (val >= 127000)
			return 127;
		return (val + 500) / 1000;
	}
}
uint16_t temp_to_u16(uint8_t data, long val)
{
	if (data & FLAG_ADT7461_EXT) {
		if (val <= -64000)
			return 0;
		if (val >= 191750)
			return 0xFFC0;
		return (val + 64000 + 125) / 250 * 64;
	} else {
		if (val <= 0)
			return 0;
		if (val >= 127750)
			return 0x7FC0;
		return (val + 125) / 250 * 64;
	}
}




