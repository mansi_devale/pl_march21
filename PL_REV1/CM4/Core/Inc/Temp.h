/*
 * Temp.h
 *
 *
 */

#ifndef INC_TEMP_H_
#define INC_TEMP_H_



#include "main.h"
#include <stdio.h>
#include <string.h>
//#include "IO.h"

#define TEMP_I2C                               hi2c1

#define FLAG_ADT7461_EXT                        1
#define TEMP_ADDR                               0x4C // 0x4D
#define Local_Temp_Value                        0x00
#define STATUS_RD_TEMP                          0x02
#define Ext_Temp_Value_HB                       0x01
#define WR_Configuration                        0x09
#define RW_Configuration                        0x03
#define WR_ConversionRate                       0x0A
#define RW_ConversionRate                       0x04

#define WR_Local_Temp_HL                        0x0B
#define RW_Local_Temp_HL                        0x05
#define WR_Local_Temp_LL                        0x0C
#define RW_Local_Temp_LL                        0x06
#define WR_Ext_Temp_HL_HB                       0x0D
#define RW_Ext_Temp_HL_HB                       0x07
#define WR_Ext_Temp_LL_HB                       0x0E
#define RW_Ext_Temp_LL_HB                       0x08

#define RW_Ext_Temp_Value_LB                    0x10
#define WR_Ext_Temp_Offset_HB                   0x11
#define RW_Ext_Temp_Offset_HB                   0x11
#define WR_Ext_Temp_Offset_LB                   0x12
#define RW_Ext_Temp_Offset_LB                   0x12
#define WR_Ext_Temp_HL_LB                       0x13
#define RW_Ext_Temp_HL_LB                       0x13
#define WR_Ext_Temp_LL_LB                       0x14
#define RW_Ext_Temp_LL_LB                       0x14

#define WR_Ext_THERM_Limit                      0x19
#define RW_Ext_THERM_Limit                      0x19
#define WR_Local_THERM_Limit                    0x20
#define RW_Local_THERM_Limit                    0x20
#define WR_THERM_Hysteresis                     0x21
#define RW_THERM_Hysteresis                     0x21
#define WR_Consecutive_ALERT                    0x22
#define RW_Consecutive_ALERT                    0x22



#endif /* INC_TEMP_H_ */
